# Mémo pour télétravail (libre)

Version 0.3 - 2020/03/20

## Préambule

Nous vivons actuellement une situation exceptionnelle.

La crise sanitaire due au COVID-19 a poussé les autorités à encourager très fortement le télétravail, ce qui implique dans l'immense majorité des cas l'usage d'outils numériques.

Mais, souvent, de mauvais réflexes (et reconnaissons-le, de mauvaises interfaces) génèrent de la frustration et de l'agacement. [Framasoft](https://framasoft.org/) dispose d’une assez longue expérience et d'une double expertise (ce qui ne veut pas dire qu'on a réponse à tout !)&nbsp;:

1. Nous connaissons des outils, nous les mettons à disposition (et parfois nous les produisons)&nbsp;;
2. Nous les pratiquons au quotidien (en tant qu'association de 35 bénévoles habitant 27 villes différentes, et une équipe de 9 salarié⋅e⋅s qui télétravaillent depuis les 5 coins de la France).

![La vidéoconférence - Allégorie](https://asso.framasoft.org/pic/3RA0JSWd/Almnj6Qd.image  "La vidéoconférence - Allégorie")


## Mise en garde

Ce document est (volontairement) biaisé : les solutions présentées ne sont quasiment que [des solutions libres](https://fr.wikipedia.org/wiki/Logiciel_libre). Cela à la fois parce qu'à Framasoft nous n'utilisons **que** des solutions libres, mais aussi (et surtout ?) parce que nous pensons que les solutions dites "propriétaires" (type Google, Microsoft, Facebook & co) sont toxiques. Cependant, au vu de la situation exceptionnelle ici, nous n'avons ni l'intention de porter un jugement sur les usages en temps de crise, ni la volonté de porter un discours politique qui nous tient pourtant à cœur. Nous présentons ces solutions tout simplement… parce que nous les utilisons.

Ce document est à destination de personnes ayant déjà des compétences de base ou intermédiaires en informatique. S'adresser au "grand public" débutant réclamerait un temps de rédaction que nous n'avons pour le moment pas.

Ce document est écrit dans l'urgence (le premier jet date du 13 mars 2020, et nous avons vécu entre temps [quelques aventures](https://framablog.org/category/framasoft/framaconfinement/)) et est donc nécessairement incomplet. Nous verrons à terme et suivant les besoins, si nous le complétons et l'enrichissons.

### Autres ressources
Au vu de la situation, Framasoft n'est pas la seule à avoir eu l'idée de produire ce genre de ressources. Nous ne doutons d'ailleurs pas un instant que ces autres ressources puissent être pertinentes, et plus détaillées que celle-ci. Nous partageons donc ci-dessous quelques liens qui nous ont semblé intéressants :

* Le site, lui aussi créé dans l'urgence, des copains et copines d'Animacoop : https://formateurs.animacoop.net/teletravail/?PagePrincipale
* Le site du collectif ContinuitéPédagogique, qui propose de la médiation en pair à pair à destination des enseignant⋅e⋅s : https://www.continuitepedagogique.org/
* Le site d'entraide autour du numérique (orienté libre) à destination des acteurs locaux stratégiques : https://gscn.eu.org/
* La page « Quelques ressources pour faciliter l’apprentissage à distance » https://www.innovation-pedagogique.fr/article6768.html

### Contribuer à ce document

Ce document est sous licence libre ([Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)) qui autorise (et encourage !) sa diffusion, sa copie et son amélioration.
Vous pouvez proposer vos améliorations sur [le forum Framasoft](https://framacolibri.org/t/ameliorer-collaborativement-le-document-memo-pour-le-teletravail-libre/7270) ou, si vous en avez les compétences, sur [notre dépôt git](https://framagit.org/framasoft/teletravail).

## Rituels

_La journée en télétravail peut nécessiter une certaine rigueur et organisation afin de se synchroniser avec le collectif, une solution facile à mettre en œuvre et à adapter est la mise en place de rituels, voici un exemple de rituel chez Framasoft._

### Début de journée

* Dire bonjour sur le tchat de l'équipe.
* Prendre un café virtuel (demander des nouvelles, petit temps informel).
* Prendre 10mn pour évaluer la journée de la veille et celle à venir.
* Planifiez vos rdv audio de la journée.
* Fixez l'heure de votre fin de journée.
* Ouvrez votre boîte mail.
* [Méthode GTD](https://fr.wikipedia.org/wiki/Getting_Things_Done) : répondez immédiatement à ce qui prend moins de 2mn. Classez le reste en fonction de ce qui prend plus de temps, et des urgences. (astuce avec le logiciel de courriel [Thunderbird](https://www.thunderbird.net/fr/) : utiliser des dossiers et/ou des codes couleurs pour trier vos messages).

### Pause

* Toutes les 2H.
* 15/20mn de pause (mettez un minuteur, vous n'aurez plus à y penser).
* Levez-vous pendant votre pause.

### Déjeuner

* En télétravail, on a souvent tendance à manger (trop) vite. Prenez votre temps.
* Faites-vous plaisir.
* Coupez *vraiment* avec le travail.

### Fin de journée

* 5/10mn avant la fin de journée, prenez le temps de prendre quelques notes : ce qui a été fait (reportez ce qui a été rayé de votre todolist), vos petites victoires, et ce qui n'a pas fonctionné.
* Dites au revoir aux collègues sur le tchat.

Essayez de conserver un espace, une disposition physique spécifique à votre télétravail. Cela peut être d'utiliser telle chaise lorsque vous travaillez (et telle autre lorsque vous jouez sur votre ordi), ou bien fermer la porte de la salle, ou telle place sur le canapé… Mais le fait d'associer une position de son corps et une disposition de son outil au moment du travail, cela permet aussi de se couper avec les préoccupations du travail lorsque vous quittez cette disposition.

Cela signifie aussi à votre entourage « si tu me vois comme ça : je bosse (donc, ne me demande pas si les nouilles au frigo sont encore bonnes) ».

**Autres sources**

* <https://www.zdnet.fr/pratique/64-conseils-pour-survivre-en-teletravail-39900263.htm>

----

## Matériel

* Un ordinateur (fixe ou portable) ;
* Téléphone (un smartphone est un plus, mais pas indispensable) ;
* Un micro-casque (celui de votre téléphone peut très bien faire l'affaire) ;
* Connexion Internet : privilégiez au maximum le filaire !! (voir la partie "[Économisez la bande passante](#economisez-la-bande-passante)") ;
* Une pièce au calme (si possible). Bureau et siège confortable, et [bien réglés](https://www.cchst.ca/oshanswers/ergonomics/office/chair_adjusting.html) ;
* Un minuteur. De préférence un physique (sinon vous allez jouer avec votre smartphone, on vous connaît !), celui de votre cuisine qui permet de savoir si les pâtes sont cuites fera très bien l'affaire. L'idée, c'est de pouvoir utiliser [la technique "Pomodoro"](https://www.ionos.fr/startupguide/productivite/technique-pomodoro/). C'est-à-dire des tâches de 15/25mn ;
* Post-its, papiers brouillons, cahier, crayons. Recommandé : agenda papier.
* Si l'environnement est bruyant : un casque audio ou des bouchons d'oreilles.

----

## Logiciels sur l'ordinateur

Les cas sont évidemment bien trop variés pour les lister tous, donc nous allons nous contenter non seulement du strict minimum, mais en plus de ne pas proposer 1000 solutions.

### Indispensables

* Navigateur récent : [**Firefox**](https://www.mozilla.org/fr/firefox/new/) (le seul dont le moteur n'est pas développé par une entreprise toxique) ;
* Suite Bureautique : [**LibreOffice**](https://fr.libreoffice.org/), qui propose une alternative complète à la suite Microsoft Office. LibreOffice comprend donc : un traitement de texte, un tableur, un outil pour créer/diffuser des diaporamas, etc.

### Recommandés

* Logiciel de mail : [**Thunderbird**](https://www.thunderbird.net/fr/).

    Oui, beaucoup de personnes utilisent Gmail. Sauf qu'en plus de vous surveiller et de renforcer la position déjà ultradominante de Google, Gmail vous oblige à être connecté en permanence, ce qui peut perturber votre concentration. Au contraire, Thunderbird permet de travailler dans un environnement où votre attention est focalisée sur l'essentiel (= pas de twitter jusqu'à côté). (+ possibilité de travailler hors-ligne). Notez qu'il est parfaitement possible de configurer une boite GMail (ou autre) préexistante sur Thunderbird ([suivez le tutoriel](https://support.mozilla.org/fr/kb/thunderbird-et-gmail)).

* Recommandé : un logiciel pour faire de copier-coller multiples. C'est le genre de logiciels méconnus qui vous change la vie. Le principe est simple : tous vos "Copier" sont enregistrés, mais en plus de la fonction "Coller" habituelle, vous pouvez accéder par une combinaison de touches à l'historique de vos "Copier" précédents, que vous pourrez alors sélectionner dans une liste. Les logiciels [**Ditto**](https://ditto-cp.sourceforge.io/) (pour Windows) ou [**Diodon**](https://launchpad.net/diodon) (pour Linux) devraient faire l'affaire.

* Recommandé : client audio : [**Mumble**](https://www.mumble.info/) (voir plus bas)

### Ressources

* l'annuaire de logiciels libres [Framalibre](https://framalibre.org).
* Le site [Alternativeto.net](https://alternativeto.net) (avec option "Free Software").

----

## Travailler à distance

### ✉️ La base : les listes de discussion

Évidemment, nous recevons trop de mails, mais c'est un outil de base pour travailler à distance, car il constitue un "plus petit dénominateur commun" : quasiment tout le monde en a un, et la plupart des gens savent l'utiliser, il n'y a pas besoin de créer un compte.

#### ⁉ C'est quoi une liste de discussion ?

Une liste de discussion, c'est une liste email (par exemple ```projet-bidule@framalistes.org```) à laquelle Alice, Bob Carole et David peuvent s'abonner. Si Bob écrit un email à la liste, les 3 autres personnes abonnées recevront l'email et pourront y répondre. L'intérêt principal étant que l'on n'est alors plus obligé de se souvenir **qui** travaille sur le projet Bidule ou de leurs adresses, et qu'on peut ajouter des abonné⋅e⋅s ou en retirer facilement.

#### Avantages des listes de discussion

* Pas besoin de créer de compte (pour les abonné⋅e⋅s) ;
* Possibilité de créer plusieurs listes et de ne s'abonner qu'à celles qui vous intéressent ;
* Pas besoin d'expliquer comment fonctionne un mail ;
* Possibilité (souvent) d'accéder à l'historique des échanges ;
* Possibilité de créer des filtres emails pour trier par listes (voir par exemple [ce tutoriel](https://support.mozilla.org/fr/kb/classer-vos-messages-en-utilisant-des-filtres)) ;
* C'est de la communication *asynchrone* : l'avantage c'est vous répondez quand vous voulez/pouvez, et c'est donc reposant !

#### Inconvénients des listes de discussion

* Le volume de mails reçu peut être important ;
* Peu de services de listes sont accessibles librement et gratuitement existent (et Framalistes est déjà débordé !). **AIDEZ-NOUS EN PROPOSANT DES SERVICES DE LISTES DE DISCUSSIONS** (basé par exemple sur le logiciel libre [Sympa](https://sympa.org)) ;
* C'est de la communication asynchrone : l'inconvénient c'est que, même si les mails arrivent généralement dans la minute, ils **peuvent mettre parfois 30mn à arriver**), et ça ne convient donc pas à des échanges rapides en mode "ping pong rapide".

#### Ressources pour les listes de discussion

* [Framalistes](<https://framalistes.org>)
* [Sympa](<https://www.sympa.org>) (le logiciel qui motorise Framalistes)

----

### 📞 La messagerie téléphonique

Là aussi, on ne va vous proposer qu'un seul logiciel (d'autres services existent) : **[Signal](https://signal.org/fr/)**.

Il s'agit d'une messagerie de type Whatsapp / Télégram, mais libre[^1], et correctement sécurisée (ce qui est censé être le cas de Whatsapp et Telegram, mais dans les faits c'est impossible à vérifier).

#### Avantages de Signal

* Possibilité de créer des groupes (avec des personnes ayant Signal)
* Messagerie et appels chiffrés (avec des personnes ayant Signal)
* possibilité de télécharger une application de bureau sur votre ordinateur (c'est plus rapide pour taper vos messages ;) )
* Notifications sur votre smartphone à réception du message (désactivable, heureusement !)

#### Inconvénients de Signal

* Il faut avoir un smartphone
* Passe par la connexion wifi ou le forfait de données dès que l'autre correspondant est sur Signal aussi

#### Ressources pour Signal

* [Site officiel de Signal pour télécharger l'application](https://www.signal.org/fr/)

----

### ☁️ Un cloud pour partager vos fichiers, agenda, contacts, visio

Le logiciel libre le plus répandu (et l'un des plus complets) est [Nextcloud](https://nextcloud.com/).

Le service [Framadrive](https://framadrive.org) (qui repose sur Nextcloud) est plein depuis longtemps, nous envisagions de sortir un projet « Framacloud » prévu pour octobre, mais le planning risque d'être chamboulé. Heureusement, de nombreux membres du [collectif CHATONS](https://chatons.org/) proposent du Nextcloud, souvent payant (quelques euros par utilisateurs et par an, mais ça en vaut la peine !), parfois gratuit (chapeau bas à [l'association La Mère Zaclys](https://www.zaclys.com/), qui en propose à des milliers d'utilisateur⋅ice⋅s, et propose même une offre gratuite aux écoles).

Évidemment, dans les structures suffisamment grandes, vous pouvez parfaitement demander à vos responsables informatiques de l'installer. Si les besoins de collaborer en temps réel sur des documents structurés sont forts, cherchez des offres proposant une suite bureautique intégrée ([Collabora Online](https://www.collaboraoffice.com/collabora-online/) ou [OnlyOffice](https://www.onlyoffice.com/fr/)), ou demandez à votre responsable informatique d'ajouter l'application.

Le fonctionnement de Nextcloud est *relativement* simple, mais le logiciel est foisonnant d'options et de plugins/applications. Nous reviendrons peut-être plus tard sur son usage avancé, mais beaucoup de documentation existe déjà en ligne, alors on vous laisse chercher !

#### Avantages de Nextcloud

* Interface relativement simple
* « Couteau suisse » du cloud : propose la gestion des fichiers, mais aussi des agendas et des contacts, synchronisation avec le téléphone, gestion (basique) de projets, etc.
* Solution très répandue, donc beaucoup de support disponible en ligne.

#### Inconvénients de Nextcloud

* Un peu d'accompagnement est le bienvenu pour commencer, car les options sont nombreuses
* Si vous avez beaucoup d'utilisateur⋅ices en ligne au même moment, Nextcloud n'est pas un foudre de guerre et peut ramer.

#### Où trouver le service Nextcloud ?

* [La documentation de Framasoft sur Nextcloud](https://docs.framasoft.org/fr/nextcloud/) et spécifiquement sur [la partie agenda](https://docs.framasoft.org/fr/agenda/).
* [La documentation pour utilisateur⋅ices de Nextcloud (en anglais)](https://docs.nextcloud.com/server/18/user_manual/)
* [S'inscrire rapidement chez un fournisseur sélectionné par Nextcloud](https://nextcloud.com/signup/)

----

### 📁 📝 La synchronisation des fichiers

L'un des gros avantages de Nexcloud, c'est qu'il dispose d'un « client de synchronisation ». Le principe est sensiblement le même qu'avec DropBox. Par exemple : tout ce que vous déposerez dans le dossier `Nextcloud/` sera automatiquement envoyé en ligne. Et tout dossier ou document créé en ligne sera automatiquement renvoyé en local sur votre machine. Pas mal, non ?

Mais c'est encore mieux quand on partage des dossiers avec les collègues.

Par exemple si Alice modifie le fichier "budget prévisionnel" (avec LibreOffice Calc, l'alternative libre à Excel, par exemple) sur son ordinateur, depuis un dossier surveillé par le client Nextcloud, il sera **automatiquement envoyé en ligne** lors de sa sauvegarde. Et il devient automatiquement disponible pour Carole et Bob, qui peuvent le modifier aussi (si Alice leur en a donné le droit).

Cela implique un peu de coordination, et notamment le fait qu'on s'organise avec les collègues en amont pour différencier les fichiers "froids" (qui sont peu modifiés) et les fichiers "chauds" (modifiés plusieurs fois par jour ou même par heure). Pour les fichiers "chauds", privilégier les pads (voir plus bas) ou la suite bureautique en ligne.

#### Avantages de la synchronisation des fichiers

Vous n'avez pas à vous préoccuper d'envoyer les fichiers en ligne : tout ce qui se trouve dans le dossier "Nextcloud" de votre ordinateur est automatiquement synchronisé en ligne.

L'inverse est vrai aussi : le client Nextcloud vérifie que votre fichier en local sur votre ordinateur a bien la dernière version disponible. Si une version plus récente est déposée en ligne, elle sera automatiquement téléchargée sur votre machine (mettant à jour votre version).

#### Inconvénients de synchronisation des fichiers

* Risque de supprimer des fichiers par inadvertance (si Bob supprime le dossier partagé d'Alice par exemple)
* Il faut déterminer en amont quel compte héberge les fichiers communs (partagés avec les autres comptes)
* Il faut installer et configurer un logiciel sur son ordinateur
* pour la synchronisation, il faut (évidemment) une connexion à Internet

#### Où trouver le client de synchronisation des fichiers ?

* [Tutoriel de Framasoft sur la mise en place du client de synchronisation](https://docs.framasoft.org/fr/nextcloud/synchro_clients.html) (pas à jour)
* [Télécharger les clients de synchronisation pour votre système](https://nextcloud.com/fr_FR/install/#install-clients)

### 📝 Les documents collaboratifs (pads)

Encore relativement méconnus, ils vous changeront la vie !

Il s'agit de pages web qui vous permettent de rédiger un texte en ligne, seul (prise de notes) ou à plusieurs (simultanément).

Les différences avec une suite bureautique en ligne ? Les pads sont + légers, + "immédiats", "compostables" (ils peuvent s'autodétruire après une période définie sans modification), ils donnent une vision claire de "qui modifie quoi ?". Usages : brainstorming, prise de notes en réunion. Chaque pad dispose de son tchat. La sauvegarde se fait automatiquement… à chaque frappe sur le clavier. Enfin, il est possible de "rembobiner" un pad dans le temps.

##### 💡 Astuce : si nécessaire, utilisez MyPads

Certains sites proposant des pads proposent aussi un plugin nommé « MyPads » (développé par Framasoft, mais que chacun⋅e peut installer) qui permet de gérer des pads publics, restreints par mot de passe, ou privés via la création de comptes et de groupes. MyPads permet aussi de gérer vos pads par dossiers, ce qui est bien pratique.

##### 💡 Astuce : Préparez votre pad de réunion en amont

Vous voulez utiliser un pad pour la prise de notes collective d'une réunion ? Excellente idée ! Lorsque vous créez un pad, il contient un texte d'aide par défaut. Supprimez-le sans remord, et indiquez quelques infos concernant la réunion :

>  **Répartition des rôles -  5mn**
>
>   + répartition de la parole/animation / Dispatch ;
>   + garant de l'ambiance ;
>   + prise de notes (enregistrer les notes et le relevé de décisions sur le nas) ;
>   + garant du temps  ;
>   + garant de l'ODJ / Aigle (s'assurer que prises de décision à la fin de chaque point/garant qu'on reste dans le sujet).
>
>    **Point météo - 5mn**
>
>    (météo perso, vide ton sac, vérifier que pas d'urgence/que les conditions sont réunies et vérifier si besoin d'un brise-glace ou d'un energizer pendant la réunion avec un panneau à dispo).
>
>    **Points à l'ordre du jour**
>
>    - Modèle : Titre du point / personne qui le propose<br/>
>      Objectif : prendre une décision, informer, discuter, enrichir…<br/>
>      Urgent  : oui/non<br/>
>      Durée :<br/>
>      Éléments de description nécessaires

Cela permettra aux participant⋅e⋅s d'être immédiatement efficaces.

#### Avantages des pads

* Pas d’inscription (sauf MyPads) pas d'installation, il suffit d'avoir l'adresse pour travailler en direct
* L'historique des modifications enregistre tout (même le « merde au prof » tapé lorsque le dos est tourné et vite effacé…)
* On peut travailler à plusieurs dizaines en même temps sur un même document
* Pas besoin de sauvegarder :)

#### Inconvénients des pads

* Si pas de MyPads (dossiers), n'importe qui peut venir voir votre travail pourvu qu'il ou elle trouve l'URL
* Utilisation mobile limitée (lecture OK, écriture bof)
* Il faut bien penser à noter l'URL de son pad, sous peine de l'oublier (et de ne jamais le retrouver ;) )
* Les options de mise en forme sont limitées (gras, italiques, souligné, titres, etc. Mais pas de tableau ou d'images dans la plupart des cas)

#### Où trouver des services de pad ?

* [Framapad.org](https://framapad.org)
* [mypads2.framapad.org](https://mypads2.framapad.org/mypads/?/subscribe) : S'inscrire sur MyPads
* [Liste des CHATONS proposant une solution de pads](https://wiki.chatons.org/doku.php/la_redaction_collaborative_avec_etherpad)

----

### 💬 Le tchat d'équipe

Successeur du vénérable [IRC](https://fr.wikipedia.org/wiki/Internet_Relay_Chat) (encore très utilisé dans certains milieux).

**Objectif** : dialogue en temps réel par "canaux de discussions" (ex.: "Projet Bidule", "Discussions budget", "Veille partagée", "Machine à café", etc.).

Le principe est simple : vous créez une équipe (par exemple "Association Machin"), vous invitez les personnes concernées. Puis chaque personne peut alors soit rejoindre un canal public (ex: "Machine à café") qui seront accessibles par tous, ou créer des canaux privés (ex : "Projet Bidule") dans lequel le créateur du canal pourra inviter uniquement les membres concernés. Ensuite, eh bien… les personnes discutent entre elles :)

#### Fonctionnalités :

* Fil de discussion organisé
* Possibilité de mentionner des utilisateurs.

    Ex: si utilisateur "michel", écrire `@michel` dans un message permet de prévenir Michel (il recevra un mail avec le message, et pourra se connecter pour répondre en ligne, ou s'il est déjà connecté, il recevra une notification à l'écran). Si [l'application Mattermost](https://mattermost.com/download/#mattermostApps) (Android/iOS) est installée et configurée sur son portable, il recevra une notification sur son smartphone (et pourra répondre depuis son smartphone).

* Possible d'ajouter des fichiers, mais pas forcément recommandé (passer par un logiciel de dépôt de fichier/image temporaire, puis copier-coller le lien)

#### Avantages du tchat d'équipe

* Discussion en « temps réel »
* Possibilité de créer des canaux de conversation
* Historique des conversations facilement accessible

#### Inconvénients du tchat d'équipe

* Risque d'être noyé dans l'information
* Un outil de plus à utiliser, avec les mails et réseaux sociaux (un compte de plus)

#### Où trouver des tchats d'équipe ?

* [Framateam](https://framateam.org/)
* [Riot/Matrix](https://about.riot.im/)
* [Liste des CHATONS proposant un tchat d'équipe](https://wiki.chatons.org/doku.php/la_discussion_instantanee_avec_mattermost)

----

### 🖼 Partage de fichiers ou d'images

Cas d'usage (gros fichier) : Alice veut envoyer un fichier vidéo à Bob, mais celui-ci pèse 50Mo et ne passe pas par email. Elle peut télécharger la vidéo sur [Framadrop](https://framadrop.org/) ou équivalent. Une fois la vidéo en ligne Alice pourra copier-coller l'adresse du fichier et l'envoyer à Bob (par email, tchat, Signal, peu importe). Le fichier s'effacera automatiquement au bout de quelques jours.

Cas d'usage (images) : Bob veut partager 5 propositions de logos à Alice et Jean. Il peut utiliser par exemple [Framapic](https://framapic.org/) ou équivalent. Il déposera ses fichiers en ligne. Framapic lui fournira une adresse d'album à partager, qu'il pourra copier-coller et envoyer à Alice (par email, tchat, Signal, peu importe). Les fichiers s'effaceront automatiquement au bout de quelques semaines/mois (configurable).

#### Avantages des outils de partage de fichiers ou d'images

* Facile à transmettre

#### Inconvénients des outils de partage de fichiers ou d'images

* Les fichiers ont un temps d'expiration défini
* Nécessite de garder trace des liens

#### Où trouver des outils de partage de fichiers ou d'images ?

##### Hébergement de fichiers

* [Framadrop](https://framadrop.org/)
* [Firefox Send](https://send.firefox.com/)
* [Liste des CHATONS poroposant le partage temporaire de fichiers](https://wiki.chatons.org/doku.php/partage-temporaire-fichiers)

##### Hébergement d'images

* [Framapic](https://framapic.org/)

----

## Prendre des décisions

Cas d'usage : Alice souhaite que son équipe valide sa vidéo de présentation de l'entreprise. En présentiel, elle aurait organisé une réunion avec les 5 membres de l'équipe, aurait bloqué une heure sur leur planning, puis le jour J à l'heure H, tout ce petit monde aurait débattu, fait remonter ses remarques, et validé (ou pas) la vidéo.

À distance, le processus est sensiblement le même, mais comme les réunions sont à éviter (cf. plus bas), le processus peut être allégé de différentes manières. Par exemple, à Framasoft, le fonctionnement général est le suivant (il peut y avoir des exceptions, l'essentiel étant de faire preuve de bon sens pour s'adapter aux situations) :

* Alice va d'abord "préparer le terrain" pour ces collègues. L'idée, c'est de leur faciliter la vie pour que la décision puisse être rapide.
  * Elle va mettre en ligne la vidéo (ex.: Framadrop, durée de vie d'une semaine) et copier son URL.
  * Puis elle va ouvrir un pad (durée de vie d'une semaine aussi), y coller l'adresse de la vidéo et préparer quelques questions et points clés auxquels ses collègues pourront répondre (par exemple "La vidéo est-elle trop longue ?", "Quel est votre sentiment général ?", etc. en laissant quelques lignes entre chaque point pour que ces collègues puissent y répondre.
  * Enfin, elle va se rendre sur le tchat d'équipe, dans le canal "Projet bidule", et poster un message du type : "Salut @channel : pourriez-vous vous rendre sur le framapad https://hebdo.framapad.org/XXXXXXXXXX quand vous pourrez. C'est pour me faire un retour sur la vidéo du projet Bidule. J'aurai besoin de vos retours avant jeudi prochain ?"
* Les collègues d'Alice vont tous et toutes recevoir une notification (grâce au `@channel` dans son message), que ce soit à l'écran, par téléphone ou par email.
* Ils pourront aller sur le pad, et télécharger la vidéo
* Ils pourront répondre aux questions d'Alice sur le pad
* Puis ils pourront retourner sur le tchat, et répondre (par exemple) "OK, c'est vu pour moi" ou "J'ai répondu sur le pad, mais j'ai un problème compliqué avec un point spécifique, on pourrait s'appeler pour en parler ?"
* Alice aura donc récupéré les retours de tous ses collègues, et le pad (hebdo) s'effacera automatiquement au bout d'une semaine sans modification, ce qui est largement suffisant (au pire, elle peut exporter son contenu en `.pdf`, `.doc`, `.odt` ou `.txt`). Idem pour la vidéo, qui s'effacera au bout d'une semaine (et n'encombrera pas de serveur).

Bon, ça, c'est le cas où tout se passe bien, mais parfois, certaines décisions sont plus compliquées à prendre.

Dans ce cas-là, on peut passer à une étape où on va demander à chacun de préciser sa position.

Par exemple, si la vidéo d'Alice enthousiasme certaines personnes, alors que d'autres sous-entendent la détester, et que 2 collègues ne se sont pas exprimés, Alice a plusieurs solutions :

**Solution 1.** Faire un vote (ce n'est pas une élection, ou un jugement sur le travail général d'Alice, mais juste un outil qui lui permettra de "lire" plus facilement la position de ses collègues) Alice peut créer un sondage avec l'outil Framadate et inviter ses collègues à y répondre de façon tranchée (ex.: "Cette vidéo vous semble-t-elle bien représenter votre travail au sein de la structure ?" Oui / Non. Puis poster l'adresse du sondage sur le tchat d'équipe. Alice recevra un mail à chaque vote (et pourra relancer les retardataires).

**Solution 2.** Lancer une recherche de consensus. Le tchat d'équipe ne suffit pas ? Les discussions par email sont interminables ? Alice peut passer à l'étape supérieure en ouvrant une discussion sur un outil comme Framavox. (Inconvénient, il faudra que chacun ait un compte sur cet outil). Ce genre d'outil est précieux pour essayer de trouver un consensus sur des problèmes complexes en essayant de "découper" ce problème en plusieurs sous-problèmes, puis en invitant chacun à se positionner dessus (au départ par texte, puis par différentes modalités de "vote" où chacun peut exprimer ses choix)

**Solution 3.** La réunion à distance. Voir ci-dessous.

----

## Se réunir à distance

Que celles et ceux qui aiment les réunions lèvent la main !
Attendez, on vous compte… Bon, vous êtes 4,2%, et comme par hasard, c'est vous qui provoquez 80% des réunions 😒

Sérieusement, la réunion à distance, ça doit être le dernier recours. Non seulement parce que c'est parfois complexe à organiser, mais aussi parce que le télétravail, ça n'est pas juste "le bureau à la maison". C'est une organisation différente du travail en présentiel. Peut-être que Bob ira chercher ses enfants à 16h, alors qu'Alice a rdv à 18h. Dans ces conditions, est-il vraiment indispensable d'organiser cette réunion à 17h ? **Détendez-vous et faites confiance à vos collègues.** Dans une immense majorité de cas, les réunions "live" ne servent à… presque rien ! Elles peuvent être remplacées par des prises de décisions spécifiques successives (voir plus haut).

Les 3 cas où les réunions "live" nous semblent importantes sont&nbsp;:

**1. Une prise de connaissance**
Par exemple la structure X veut vous présenter son projet. Vous avez préalablement demandé à la structure X de vous faire un mail pour vous présenter les choses dans les grandes lignes. À la lecture du mail, vous êtes intéressé. Du coup, vous convenez d'un appel téléphonique ou d'une visio à 2.

**2. Les transmissions d'informations**
Il s'agit, typiquement, des réunions d'équipe. Le rythme ne doit pas être trop important (pas la peine de mobiliser 6 personnes tous les jours ! Si vous êtes transparents sur le tchat d'équipe, chacun saura ce qu'ont fait les autres (ou pourra poser la question quelques jours plus tard), ni trop faible (une réunion d'équipe par mois, c'est la meilleure façon de laisser les personnes "en plan" sans qu'elles puissent exprimer leurs difficultés, leurs satisfactions, leurs frustrations, etc.). Une réunion par semaine semble un bon rythme, mais à vous de le trouver !

**3. Les dissensus inextricables**
Toute organisation rencontre de temps à autre des situations (souvent liées au Putain de Facteur Humain) qui n'ont pas trouvé de solution par les processus de décisions classiques. Dans ce cadre-là, organiser une réunion (et bien la préparer en amont !) peut être une solution.

## Préparer (et réussir) une réunion à distance

### Rappel sur le nombre de participant⋅e⋅s

Plus de 10 ? Cette réunion "live" est sans doute une mauvaise idée ! Associations : sachez qu'il est parfaitement possible, si vos statuts le permettent, d'organiser des A.G. en ligne, sans visio ! À Framasoft, nous pratiquons régulièrement ce type d'AG à distance, en passant par notre tchat d'équipe, et où les 35 membres de l'association s'expriment à l'écrit uniquement. Et ça marche très bien, à condition d'avoir bien préparé les choses en amont.

### Une réunion, ça se prépare !

Enfonçons une porte ouverte, mais une réunion sans Ordre Du Jour est vouée à l'échec. Quitte à ce que cet ordre du jour change en cours de réunion (ce n'est pas interdit !), mais vos interlocuteurs doivent savoir ce qu'ils font là, et doivent avoir un "guide" sous les yeux. Minuter l'ODJ est super important.

Une bonne façon de préparer en amont :

* Désigner un⋅e "facilitateur⋅ice" (souvent vous) qui se chargera : des invitations, des tests préalables (voir ci-dessous), de l'accueil des participants, de s'assurer que chacun est à l'aise avec les outils, du respect des horaires.
* Désigner un⋅e "animateur⋅ice" qui veillera au respect de l'horaire et du suivi de l'ordre du jour (chez nous on a : un⋅e garant⋅e de l'ordre du jour (qui vérifie qu'on ne s’éloigne pas du sujet), un⋅e ou plusieurs scriptes pour les notes (dont une personne qui s'assure que les notes sont exportées ou sauvegardées) , un⋅e dispatcheur qui distribue la parole ou la donne à celle et ceux qui ne la prennent pas spontanément, ce qui est encore plus important que d'habitude à distance, un⋅e garant⋅e du temps). Je vous colle en bas du pad le début notre pad "réunion d'équipe"
* Lancer les invitations (par exemple avec un framadate) au moins 15j avant
* Ouvrir un pad spécifique à la réunion
* Lister sur ce pad la date, la liste des participants (que chacun devra compléter, ne serait-ce que pour leur rappeler qu'ils peuvent agir sur le pad), les heures de début et de fin (prévues et réelles) de la réunion, l'ordre du jour, les adresses Internet qui pourraient être utile.

#### Les points à vérifier en amont

Assurez-vous d'**être au calme** (de préférence dans un bureau fermé plutôt que dans un café !) et de ne pas être dérangé. Coupez la sonnerie de votre téléphone. N'ouvrez *que* les logiciels et pages web nécessaires (= fermez Twitter !).

**Il faut ABSOLUMENT tester votre matériel avant.** Si vous donnez rendez-vous à 14 h sur Mumble, et que vous installez le logiciel à l'arrache à 13h58, c'est perdu : à 14h15, vous serez encore à essayer de configurer votre micro. N'hésitez pas à tester plusieurs jours avant. Une fois que vous serez rodés, vous pourrez vous connecter 2 mn avant. Mais si vous débutez, exercez-vous ! (par exemple avec des collègues ou amis).

**Chaque participant doit être *fortement invité* à tester son matériel avant.** Vous n'inviteriez pas des gens venant de toute la France à une réunion en leur donnant l'adresse du rdv 5mn avant le début ? Eh bien ne faites pas la même chose en ligne ! Par exemple, si votre réunion a lieu sur Mumble, proposez aux participants, plusieurs jours avant (!), de les accompagner dans l'installation et la configuration du logiciel. Une fois qu'ils auront fait 3 ou 4 réunions, cela ne sera plus nécessaire. Mais si vous ne le faites pas, **soyez assuré que les 45 premières minutes de réunions seront passées à aider Gérard à configurer sa carte son**. Ce qui créera de la frustration.

Vous utilisez de l'audio (ou de la vidéo) ? ** Imposez le micro-casque !** La plupart des logiciels (voire la plupart des micros et carte son) disposent de dispositifs de réduction d'écho et de réduction de bruits ambiants. Mais leur qualité est extrêmement variable ! Du coup, c'est le larsen assuré, ou la forte probabilité d'entendre votre propre écho lorsque vous parlez, tout simplement parce que votre douce voix sortira sur les enceintes de Bob, et que son micro (pensant que c'est Bob qui parle) vous la renverra en écho. J'insiste : imposez le micro-casque à chaque interlocuteur. Tout le monde dispose de ce genre de casque, vendu d'office avec les téléphones. Pas besoin d'investir dans de la haute technologie, le moindre micro-casque à 10€ fera très bien l'affaire, et est déjà conçu pour réduire les bruits ambiants, et évite les problèmes de larsen.

En début de réunion, **faites une "météo" des personnes**, en les invitant chacun à leur tour, à se présenter et/ou à s'exprimer en quelques mots sur comment ils vont. L'objectif est quadruple :

1. s'assurer que chacun est dans de bonnes conditions pour la réunion (ou comprendre pourquoi ça ne sera peut-être pas le cas). Par exemple, si Bob vient d'apprendre une heure auparavant qu'il devait garder son enfant malade, il sera probablement préoccupé et pas forcément concentré, mais au moins vous saurez pourquoi.
2. le non verbal, c'est important, mais en audioconf, c'est… impossible ! (et en visio c'est à peine mieux). Pendant que chacun se présente, vous pouvez repérer des intonations de voix différentes, qui pourront vous mettre la puce à l'oreille.
3. parfois, tout le monde ne se connaît pas, et faire un rapide tour de table permet à chacun d'identifier les voix des personnes présentes. Bien de doubler ce tour de table sonore avec le pad (chaque personne écrit son prénom, nom, etc. sur le pad, ça permet aux nouveaux arrivant⋅e⋅s de voir qui est présent sans que l'animateur⋅trice doive refaire la liste, et ça permet aux participant⋅e⋅s de se familiariser avec le pad, de voir la magie d'écrire en direct et de voir écrire les autres ;)).
4. cela permet enfin de vérifier que les outils fonctionnent correctement (les téléphones, les micros, les pads, etc.)

Après ce tour de table, invitez le facilitateur à rappeler les règles, même succinctement : on ne se coupe pas la parole, heure de fin, adresse du pad, le fait que la prise de note est collaborative, etc.

**N'ayez pas peur de "kicker" quelqu'un !** Bob ne joue pas le jeu et se pointe à la réunion sans avoir testé son matériel ? En conséquence, il fait perdre 10 mn à 10 autres personnes et, en plus, émet des bruits bizarroïdes en permanence empêchant d'être concentré sur la conversation ? Alors, virez Bob ! Désolé pour Bob, mais dans une réunion physique on n'arrive pas avec des enceintes jouant de la musique forte. Bob n'est pas prêt ? Tant pis. Demandez-lui de partir et de revenir quand il aura fait en sorte de régler ses problèmes. (méthode avec Muéble et Jitsi)

**Ne parlez pas tous en même temps !** Un autre calvaire des réunions à distance : les gens qui se coupent la parole et qu'on ne reconnaît même pas, les gens qui toussent, les klaxons de la rue qui vont être entendus par tout le monde. Le principe : l'animateur⋅ice, en début de réunion (et pendant si nécessaire) rappelle que les personnes qui ne prennent pas la parole doivent couper leur micro. Et que les personnes qui prennent la parole doivent redire leur prénom avant chaque intervention.

**Sur la plupart des téléphones**, il y a souvent une fonctionnalité pour couper votre micro (le plus souvent située à côté du mode "haut-parleur"). **Sur Mumble**, vous pouvez (devez, même !) utiliser le mode "push to talk" : vous pouvez configurer une touche (ou une combinaison de touches) de votre clavier pour activer ce mode de communication. Tant que cette/ces touches ne sont pas appuyées, vous n'émettez aucun son (vous pouvez donc renifler tranquillement ;-) ). Lorsque vous pressez cette touche, votre micro s'ouvre, et votre voix sera transmise à tout⋅e⋅s les participant⋅e⋅s du salon Mumble. **Sur Jitsi/Framatalk**, vous avez une option en bas de l'écran qui vous permet de désactiver ou réactiver votre micro d'un simple clic. Pour demander la parole, vous pouvez cliquer sur l'icône "Main" qui signalera à tout le monde que vous souhaitez vous exprimer.

Nous sommes conscient⋅e⋅s que cela n'est pas simple lors des premières prises de paroles, et qu'il y a un coup à prendre, mais cela vaut vraiment la peine de s'adapter à ces nouvelles pratiques.

…Mais veillez à ce que chacun⋅e puisse s'exprimer. Comme les interlocuteur⋅ice⋅s ne se voient pas, il faut là aussi faire preuve de discipline. Chacun⋅e, en fin de prise de parole, doit laisser quelques secondes à ses interlocuteurs pour s'exprimer (notamment pour qu'ils et elles puissent avoir le temps de réactiver leur micro).


### Réunion audio / audio conférence

Dans de très nombreux cas, l'image n'est pas du tout indispensable. Loin de nous l'idée qu'elle n'a pas d'intérêt, ne serait-ce que pour la communication non-verbale, mais posez-vous vraiment la question avant : ai-je besoin de voir mes interlocuteurs ?
Par ailleurs, ne pas diffuser d'images, c'est aussi avoir la capacité de se concentrer sur autre chose que son image-de-soi. Vous pouvez vous gratter le nez, lire un article, bailler tranquillement…

Rappels :

* N'hésitez pas à utiliser https://framadate.org pour caler la date et l'heure du rendez-vous en amont
* Quelques heures avant la réunion, notifiez vos interlocuteur⋅ice⋅s sur le tchat d'équipe pour leur rappeler l'heure et les identifiants de la réunion
* Pour la réunion téléphonique, créez en amont un "pad" (voir + haut) avec la date, l'intitulé, et l'ordre du jour de la réunion


#### Moins de 10 personnes à réunir ? Utilisez le bon vieux téléphone

Alors on sait, à l'heure de WhatsApp ou de Discord, le téléphone, ça fait un peu old-school.
Pourtant, c'est un outil extrêmement efficace, et qui demande peu de compétences techniques.

Cas d'usage : Alice a besoin de discuter les stratégies de communication du  projet Bidule. Bien entendu, elle a ses pads, le tchat d'équipe, etc. Mais là, elle veut un brainstorming sur la pertinence de ce qu'elle veut présenter. La réunion est prévue à 11h avec Bob, Carole, Denis et Jean.

Vers 9H, Alice se rend sur https://ovh.com/conferences et réserve une conférence audio en laissant son adresse mail. Elle reçoit quasi-instantanément un email contenant le numéro de téléphone à appeler et l'identifiant de connexion à la conférence (valable 24h). Elle envoie alors à ses collègues :

* le numéro de téléphone et l'identifiant de connexion
* l'adresse du pad de la réunion (voir "[Préparer votre pad de réunion](#💡-Astuce--Préparez-votre-pad-de-réunion-en-amont)")
* le rappel de l'heure de la réunion

Ainsi, à 11h, tous et toutes peuvent appeler le n° de téléphone pour se parler au téléphone, et faire une prise de notes collaboratives sur le pad.

##### Avantages du téléphone

* Le téléphone, tout le monde sait comment ça marche
* Pas de mise en place technique compliquée, c'est un véritable atout pour les personnes peu à l'aise avec le numérique
* Cette solution est souvent bien plus robuste et résiliente que de passer par des outils en ligne (par exemple, ça fonctionne sans internet :) )

##### Inconvénients du téléphone

* Avertissement : lire "[préparer et réussir une réunion à distance](#preparer-et-reussir-une-reunion-a-distance)"
* Coût d'une communication téléphonique standard, c'est-à-dire le plus souvent gratuit à l'intérieur du pays, mais peut être payant si des participants viennent d'autres pays

##### Ressources sur les conférences audio par téléphone

* <https://ovh.com/conferences>
* (sans doute d'autres, vous pouvez nous les signaler [sur le forum](https://framacolibri.org/t/ameliorer-collaborativement-le-document-memo-pour-le-teletravail-libre/7270) )
* Appel classique multi-participants ? Supporté sur certains téléphones ?

----

#### Plus de 10 personnes ? Utilisez Mumble

Déjà, c'est probablement une mauvaise idée d'avoir plus de 10 participant⋅e⋅s. Si c'est pour prendre une décision, faites un sondage ou utilisez des outils de recherche de consensus.

Mumble est une alternative à Discord ou TeamSpeak

Attention, ce n'est pas un logiciel web : il faut que chaque participant installe le logiciel client avant. Pour un bon fonctionnement de la réunion, il est **fortement recommandé** d'avoir pris le temps de configurer Mumble selon vos besoins et d'avoir fait des tests en amont avec vos participant⋅e⋅s.

Important : utiliser de préférence la fonctionnalité "Push to talk" (votre micro est automatiquement fermé, sauf lorsque vous appuyez sur une touche prédéfinie).

##### Avantages

* Peut fonctionner avec un nombre très important de participant⋅e⋅s (100 et +)
* Bonne qualité audio (meilleure que le téléphone si vous respectez les règles)
* Mumble dispose d'un tchat intégré (par exemple pour y copier un lien ou dire que vous voulez prendre la parole)
* Possibilité d'enregistrer la conversation (prévenez vos interlocuteurs en amont que la conversation sera enregistrée)
* [Application Android "Plumble"](https://play.google.com/store/apps/details?id=com.morlunk.mumbleclient.free&hl=fr)) / [Application iPhone](https://apps.apple.com/fr/app/mumble/id443472808) disponible aussi. Cela ne dispense pas d'activer le mode "push to talk" ou de couper son micro quand on ne participe pas.
* Si tou⋅te⋅s les partipant⋅e⋅s sont rôdé⋅e⋅s à l'usage de l'outil, votre réunion pourra être beaucoup plus agréable qu'une réunion téléphonique

##### Inconvénients

* Avertissement : lire "[préparer et réussir une réunion à distance](#preparer-et-reussir-une-reunion-a-distance)"
* La configuration peut parfois être compliquée pour des utilisateur⋅ice⋅s peu habitué⋅e⋅s au numérique (l'interface est vieillotte et il est parfois difficile de s'y retrouver).
* Si les utilisateur⋅ice⋅s n'ont jamais utilisé ce genre d'outil, il est indispensable de les accompagner une première fois en amont, sinon vous passerez votre réunion à régler leurs problèmes techniques.

##### Ressources

* [Tutoriel Mumble de Framasoft](https://docs.framasoft.org/fr/jitsimeet/mumble.html)
* [L'article annonçant le Mumble de Framasoft](https://framablog.org/2020/03/19/mumble-framatalk-un-serveur-pour-parler-a-plusieurs/)
* [Serveur Mumble de Framasoft](https://mumble.framatalk.org)
* [Serveur Mumble de l'APRIL](https://wiki.april.org/w/Mumble)
* [Guide sur le wiki des Chatons](https://wiki.chatons.org/doku.php/la_conference_telephonique_avec_mumble)

----

### Réunion vidéo / Visio conférence

[![Les écueils de la visio dans la vraie vie](https://asso.framasoft.org/pic/fdA4OKBL/FYxGmDwJ.png  "Les écueils de la visio dans la vraie vie")](https://aperi.tube/videos/watch/09480e54-263a-4fec-9c97-b793e397ec0e?lang=fr)

_Vidéo : "Les écueils de la visio dans la vraie vie" © Tripp and Tyler_


Le monde du libre dispose de quelques outils pour faire de la visio-conférence (par exemple [BigBlueButton](https://bigbluebutton.org/) ou [OpenMeeting](https://openmeetings.apache.org/)) et d'autres pour diffuser un flux vidéo live de 1 personne vers X personnes ([Opencast](https://opencast.org/), ou peut être bientôt [PeerTube](https://joinpeertube.org)). Cependant, ces outils sont parfois complexes à installer (sauf PeerTube, mais nous ne sommes pas très objectifs ;) ).
Un outil plutôt efficace se nomme **JitsiMeet**.

Rappelons (encore une fois) que la vidéo n'est pas **du tout** la panacée pour une réunion à distance. Nous avons naturellement tendance à vouloir reproduire le modèle d'une réunion physique (non seulement pour bénéficier des informations de la communication non-verbale, mais aussi par habitude), or la visio-conférence n'a pas que des avantages :

* techniquement, elle est parfois plus compliquée à mettre en place (webcam qui plante, matériel incompatible, etc)
* techniquement elle peut mal fonctionner, et rien de plus frustrant que de devoir faire répéter son interlocuteur 5 fois
* comme nous savons que les autres peuvent nous voir, nous agissons différemment (en plus de l'attention au sujet de la réunion, il faut prêter attention à son image)
* plus grande utilisation de la bande passante = plus de risques de plantage
* etc.

Cependant, une visio-conférence peut parfois s'avérer utile, notamment lorsque l'on souhaite créer du lien humain (par exemple si l'une des personnes de l'équipe ne va pas bien).

Cas d'usage : Alice et Bob ont eu connaissance qu'une personne qu'ils ne connaissent pas, François, souhaite apporter son aide à leur projet Bidule.

* Alice prend contact en amont avec François pour s'assurer qu'il a déjà utilisé l'outil JitsiMeet et qu'il a le matériel adéquat (normalement, un simple navigateur réçent, ou l'application "JitsiMeet" installée sur son téléphone, suffisent).
* Le jour J, 10mn avant la réunion, Alice se rend sur https://framatalk.org (par exemple) et ouvre un salon de discussion (par exemple "projetBidule-reunion-francois"). Elle récupère l'adresse donnée, par exemple `https://framatalk.org/projetbidule-reunion-francois` et l'envoie à Bob et François.
* Chaque participant peut alors accéder à la conférence, après avoir accepté que son navigateur web puisse accéder temporairement à sa webcam et son micro.

#### Avantages

* Rien à télécharger !
* Pas de compte nécessaire !
* Possibilité d'ajouter un mot de passe
* Possibilité de partager son écran aux autres participant⋅e⋅s
* Possibilité d'utiliser un tchat intégré
* Possibilité d'utiliser un bouton "Demande de parole"
* il existe des applications pour smartphone

#### Inconvénients

* JitsiMeet (le logiciel qui motorise Framatalk) est performant à 2 interlocuteur⋅ice⋅s (face à face), **mais plus il y a d'interlocuteur⋅ices moins bien il fonctionnera** (il est déconseillé d'être plus de 4 participant⋅e⋅s, même si cela **peut** fonctionner)
* Il y a actuellement un [bug identifié](https://github.com/jitsi/jitsi-meet/issues/4758) bug identifié avec certaines versions de Firefox, donc testez le logiciel avant

#### Ressources

* [Le site officiel de visio-conférence](https://meet.jitsi.net/)
* [Le site officiel du logiciel et de la communauté Jitsi](https://jitsi.org) (en anglais)
* [_L'instance_ Framasoft de Jitsi](https://framatalk.org)
* [La liste des CHATONS proposant une instance Jitsi](https://wiki.chatons.org/doku.php/la_visio-conference_avec_jitsi)
* [_L'instance_ Scaleway de Jitsi](https://ensemble.scaleway.com/)

----

## Trucs et astuces

### Économisez la bande passante !

Pourquoi économiser la bande passante ?
Le problème de fond, ce n'est pas vraiment la capacité globale des tuyaux de l'Internet mondial (ou même national). Globalement, ils sont plutôt bien dimensionnés et devraient être suffisants durant la période de confinement.
Le souci, c'est plutôt "le dernier kilomètre avant l'ordinateur". Si des dizaines de milliers de personnes, au même endroit, téléchargent une série en 4G, vous pouvez être sûr que l'antenne téléphonique ne suivra pas, et que le débit Internet deviendra asthmatique. Idem chez vous, si vous êtes 10 sur le même réseau WiFi surfer ou écouter de la musique en streaming, cela pourrait impacter votre capacité à faire une visio-conférence dans de bonnes conditions.

Bref, quelques bonnes pratiques permettent de faire preuve de sobriété en termes d'utilisation du réseau. Vous ferez un (petit) geste pour la planète, mais vous aurez surtout la possibilité d'être beaucoup moins frustré⋅e dans votre pratique numérique.

Chez vous, vous voulez travailler en musique ? OK !
Mais évitez de laisser Youtube jouer une playlist dans un onglet de votre navigateur ! Idem pour Spotify ou Deezer : téléchargez les albums que vous écoutez le plus souvent, vous laisserez de la bande passante disponible pour d'autres usages. Pensez, pourquoi pas, à écouter la radio ou à ressortir CD ou vinyles.
Côté vidéo, nous comprenons que le "Netflix & chill" peut détendre. Mais comme pour la musique, pensez plutôt à ressortir vos DVD (faites une bourse d'échanges entre voisins) ou… lisez !

Enfin, même si c'est moins impactant, évitez de garder Facebook ouvert toute la journée : même lorsque vous ne regardez pas.

<https://www.arcep.fr/demarches-et-services/utilisateurs/teletravail-et-connexion-internet.html>

### Conseils pour une réunion Mumble efficace

* IMPORTANT : prévoyez bien d'**activer l'option “push to talk”** avant de rejoindre le salon.
L'option s'active dans "Configurer > Paramètres > Entrée Audio > Transmission". Puis définissez un raccourci via "Configurer > Paramètres > Raccourci" :

   1. choisissez "Appuyer pour parler" dans la colonne de gauche ("Fonction")
   2. cliquez en face dans la colonne de droite ("Raccourci"), le texte "Appuyer sur un raccourci clavier" apparaît
   3. sélectionnez la touche qui - tant qu'elle sera appuyée - ouvrira votre micro et vous permettra d'être entendu. (par exemple, la touche "Ctrl" à proximité de la barre espace est souvent peu utilisée, cela peut être un bon choix)

* IMPORTANT : **prenez un micro-casque** (celui basique, de votre téléphone fera très bien l’affaire), ça évitera le larsen et les échos

* IMPORTANT : veillez à être dans un **environnement sonore aussi calme que possible**.

* Point technique : il est bienvenu de **désactiver la synthèse vocale** dans mumble (= ce qui est posté dans le tchat Mumble est lu par une voix synthétique) : il n'y a que vous qui l'entendrez : mais 1) votre micro peut la capter le son et le renvoyer (larsen), 2) ça couvre le son des autres voix.

* N’hésitez pas à arriver **10mn en avance**, justement pour prendre le temps de faire tous les réglages nécessaires et tester en live avec d’autres.

* Lorsque vous prenez la parole, **annoncez-vous** (ex: « pyg de Framasoft : Oui, je suis d'accord, et j'aimerai ajouter que blablabla » ) : 1) ça facilite la compréhension et la prise de notes, 2) en mode "activité vocale, ça permet d'éviter de perdre le début du contenu"

* **Évitez de couper la parole aux autres** (c'est possible, hein, mais merci de le faire avec modération et réflexion)

* Contrepartie du point précédent : Faites des **interventions courtes**, et "redonnez" la parole (ex: « blablabla. J'ai fini. » )

* Prêtez attention à ce que chacun⋅e puisse s'exprimer (même si ce n'est pas vous l'animateur⋅ice)

----

## À propos de ce document

### Licence

Ce document est publié sous licence Creative Commons BY-SA

### Auteurs et autrices

* https://framagit.org/framasoft/teletravail/-/graphs/master ainsi que les personnes ayant proposé des modifications sur le [sujet de forum dédié](https://framacolibri.org/t/ameliorer-collaborativement-le-document-memo-pour-le-teletravail-libre/7270).



[^1]: Pas totalement, diront les esprits chagrins, et ils auront bien raison.
